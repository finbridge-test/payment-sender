<?php
/**
 * Created by PhpStorm.
 * User: imshodan
 * Date: 18.04.20
 * Time: 22:53
 */

return [
    'receiver_uri' => env('PAYMENT_URL', 'http://127.0.0.1:8001')
];
