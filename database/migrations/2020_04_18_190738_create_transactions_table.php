<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void {
        Schema::create('transactions', function(Blueprint $table) {
            $table->string('id', 64)->primary()->nullable(false)->comment('Идентификатор транзакции');
            $table->float('sum', 5, 2)->nullable(false)->comment('Сумма');
            $table->float('commission', 5, 2)->nullable(false)->comment('Комиссия');
            $table->tinyInteger('order_number', false, true)->nullable(false)->comment('Идентификатор клиента');

            $table->index('order_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void {
        Schema::dropIfExists('transactions');
    }
}
