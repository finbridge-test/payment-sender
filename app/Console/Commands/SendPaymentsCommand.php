<?php
/**
 * Created by PhpStorm.
 * User: imshodan
 * Date: 18.04.20
 * Time: 12:59
 */

namespace App\Console\Commands;

use App\Models\Transaction;
use Exception;
use Faker\Generator;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Console\Command;
use Signatory\Interfaces\SignatoryInterface;

/**
 * Class SendPaymentsCommand.
 *
 * Генерирует случайное количество платежей и отсылает подписанный запрос к приемнику платежей.
 *
 * @package App\Console\Commands
 */
class SendPaymentsCommand extends Command {

    /**
     * @var string
     */
    protected $signature = 'send:payments {--interval=20 : Интервал между отправкой пакетов}';

    /**
     * @var string
     */
    protected $description = 'Генерирует случайное количество платежей и отсылает подписанный запрос к приемнику платежей.';

    /**
     * @var SignatoryInterface
     */
    protected $signatory;

    /**
     * @var Generator
     */
    protected $faker;

    /**
     * @var string
     */
    protected $receiverUri;

    /**
     * SendPaymentsCommand constructor.
     *
     * @param SignatoryInterface $signatory
     * @param Generator $generator
     */
    public function __construct(SignatoryInterface $signatory, Generator $generator) {
        parent::__construct();

        $this->signatory   = $signatory;
        $this->faker       = $generator;
        $this->receiverUri = config('payment.receiver_uri');
    }

    /**
     * @return void
     * @throws Exception
     */
    public function handle(): void {
        $interval = $this->option('interval');
        $interval = is_numeric($interval) ? (int) $this->option('interval') : 20;

        $this->output->title('Эмулятор платежной системы');
        while (true) {
            $package = [];
            $random  = $this->faker->numberBetween(1, 10);
            $this->output->note('Генерируем случайные транзакции. Количество = ' . $random);
            for ($i = 0; $i < $random; $i++) {
                /** @var Transaction $package */
                $transaction           = $this->createTransaction();
                $serializedTransaction = '';
                foreach ($transaction as $key => $value) {
                    $serializedTransaction .= "$key = $value, ";
                }

                $this->output->text(substr($serializedTransaction, 0, -2));
                $package[] = $transaction;
            }

            Transaction::query()->insert($package);
            $this->sendPackage($package);
            sleep($interval);
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    protected function createTransaction(): array {
        return [
            'id'           => $this->faker->unique()->uuid,
            'sum'          => $this->faker->randomFloat(1, 10, 500),
            'commission'   => $this->faker->randomFloat(1, 0.5, 2),
            'order_number' => $this->faker->numberBetween(1, 20)
        ];
    }

    /**
     * @param array $data
     * @return void
     */
    protected function sendPackage(array $data): void {
        $sign = $this->signatory->sign($data);
        $this->output->note('Цифровая подпись пакета данных = ' . $sign);
        $headerName = config('signatory.header_name');

        $client  = new Client();
        $headers = [
            $headerName    => $sign,
            'Content-Type' => 'application/json'
        ];

        $request = new Request('POST', $this->receiverUri, $headers, json_encode($data));
        try {
            $response = $client->send($request);
            if ($response->getStatusCode() === 200) {
                $this->output->success($response->getReasonPhrase());
            }
        } catch (ClientException $exception) {
            if ($response = $exception->getResponse()) {
                $this->output->error($response->getBody()->getContents());
            }
        }
    }

}
