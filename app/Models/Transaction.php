<?php
/**
 * Created by PhpStorm.
 * User: imshodan
 * Date: 18.04.20
 * Time: 22:29
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction.
 *
 * @property string $id
 * @property float $sum
 * @property float $commission
 * @property int $order_number
 *
 * @package App\Models
 */
class Transaction extends Model {

    /**
     * @var string
     */
    protected $table = 'transactions';

    /**
     * @var array
     */
    protected $fillable = ['id', 'sum', 'commission', 'order_number'];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;
}
